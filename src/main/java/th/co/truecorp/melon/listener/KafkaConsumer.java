package th.co.truecorp.melon.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import th.co.truecorp.melon.model.MelonNotification;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "Kafka_Example")
    public void consume(String message) {
        System.out.println("Consumed message: " + message);
    }


    @KafkaListener(topics = "Kafka_Example_json", containerFactory = "userKafkaListenerFactory")
    public void consumeJson(MelonNotification message) {
        System.out.println("Consumed JSON Message: " + message);
    }
}